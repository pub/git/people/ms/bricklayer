###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import itertools
import logging

from . import step
from .i18n import _, N_

# Setup logging
log = logging.getLogger("bricklayer.bootloaders")

GRUB_TIMEOUT = 5

class Bootloader(object):
	"""
		A generic class for bootloaders
	"""

	# Set the name of the bootloader
	name = None

	# Which architectures are supported?
	arches = []

	# Packages that need to be installed
	packages = []

	# Requires a BIOS boot partition?
	requires_bootldr_partition = False

	# Requires an EFI partition?
	requires_efi_partition = False

	def __init__(self, bricklayer):
		self.bricklayer = bricklayer

	@property
	def supported(self):
		"""
			Returns True if this booloader is supported
		"""
		# If arches is empty, this bootloader is supported
		if not self.arches:
			return True

		return self.bricklayer.arch in self.arches

	def install(self):
		"""
			Called to install the bootloader
		"""
		pass


class Grub(Bootloader):
	name = N_("GRUB")

	arches = [
		"x86_64",
	]

	packages = [
		"grub",
	]

	requires_bootldr_partition = True

	def install(self):
		# Write the configuration file
		self.write_configuration()

		# Install GRUB2 on all disks that have been selected
		for disk in self.bricklayer.disks.selected:
			self.install_on_disk(disk)

		# Install configuration
		self.install_configuration()

	@property
	def grub_arch(self):
		return "i386-pc"

	@property
	def grub_args(self):
		return [
			"--target=%s" % self.grub_arch,
		]

	def install_on_disk(self, disk):
		log.info("Installing GRUB on %s" % disk.path)

		self.bricklayer.command([
			"grub-install", "--verbose", "--force", "--recheck",
			*self.grub_args, disk.path,
		], chroot=True, bind=["/dev", "/proc", "/sys"])

	def write_configuration(self):
		"""
			This method writes /etc/default/grub
		"""
		log.debug("Writing GRUB Configuration:")

		# Fetch the kernel commandline
		kernel_cmdline = self.bricklayer.settings.get("kernel-cmdline", []).copy()

		conf = {
			# Tell GRUB who we are
			"GRUB_DISTRIBUTOR" : "\"$(sed 's, release .*$,,g' /etc/system-release)\"",

			# Give the user a moment to select an option
			"GRUB_TIMEOUT" : GRUB_TIMEOUT,

			# Remember the selected option
			"GRUB_DEFAULT" : "saved",

			# Do not show a submenu
			"GRUB_DISABLE_SUBMENU" : "true",

			# Disable the recovery option
			"GRUB_DISABLE_RECOVERY" : "true",
		}

		# Enable the serial console
		if self.bricklayer.settings.get("serial-console"):
			device   = self.bricklayer.settings.get("serial-console-device")
			baudrate = self.bricklayer.settings.get("serial-console-baudrate")

			# Find out on which console we are running
			unit = device.removeprefix("ttyS")

			conf |= {
				"GRUB_TERMINAL_OUTPUT" : "\"serial console\"",
				"GRUB_SERIAL_COMMAND"  : "\"serial --unit=%s --speed=%s\"" % (unit, baudrate),
			}

			# Append the serial console to the kernel commandline
			kernel_cmdline.append("console=%s,%sn8" % (device, baudrate))

		# Otherwise enable a generic VGA console
		else:
			conf["GRUB_TERMINAL_OUTPUT"] = "\"console\""

		# Set the kernel commandline
		if kernel_cmdline:
			conf["GRUB_CMDLINE_LINUX"] = "\"%s\"" % " ".join(kernel_cmdline)

		# Write everything to file
		with self.bricklayer.open("/etc/default/grub", "w") as f:
			for key, val in conf.items():
				# Log the line
				log.debug("    %s = %s" % (key, val))

				# Write the line to file
				f.write("%s=%s\n" % (key, val))

	def install_configuration(self):
		"""
			Generates a GRUB configuration file
		"""
		self.bricklayer.command([
			"grub-mkconfig", "-o", "/boot/grub/grub.cfg",
		], chroot=True, bind=["/dev", "/proc", "/sys"])


class GrubEFI(Grub):
	name = N_("GRUB EFI")

	requires_efi_partition = True

	@property
	def grub_arch(self):
		if self.bricklayer.arch == "aarch64":
			return "arm64-efi"

		elif self.bricklayer.arch == "x86_64":
			return "x86_64-efi"

	@property
	def grub_args(self):
		return [
			"--target=%s" % self.grub_arch,
			"--efi-directory=/boot/efi",
		]

	@property
	def supported(self):
		# XXX currently disabled, because the GRUB package is broken for EFI
		return False


class InstallBootloader(step.Step):
	"""
		Installs the bootloader
	"""
	def run(self):
		# Find all packages that need to be installed
		packages = []
		for bootloader in self.bricklayer.bootloaders:
			packages += bootloader.packages

		# Install them
		if packages:
			self.install_packages(packages, title=_("Installing Bootloader"))

		# Install all bootloaders
		for bootloader in self.bricklayer.bootloaders:
			with self.tui.progress(
				_("Installing Bootloader"),
				_("Installing bootloader \"%s\"...") % _(bootloader.name),
			):
				bootloader.install()
