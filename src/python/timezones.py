###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import pytz

from . import step
from .i18n import _

# Setup logging
log = logging.getLogger("bricklayer.timezones")

class SelectTimezone(step.InteractiveStep):
	first_install = True

	def run(self):
		# Get a list of all available timezones
		timezones = {
			tz : tz for tz in sorted(pytz.all_timezones)
		}

		# Which timezone is currently selected?
		timezone = self.bricklayer.settings.get("timezone", "UTC")

		timezone = self.tui.select(
			_("Timezone"),
			_("Please select the timezone:"),
			timezones, default=timezone,
			height=10,
		)

		log.info("Selected timezone: %s" % timezone)

		# Save in settings
		self.bricklayer.settings["timezone"] = timezone
