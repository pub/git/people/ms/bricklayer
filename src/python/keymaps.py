###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2023 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging

from . import step
from .i18n import _

# Setup logging
log = logging.getLogger("bricklayer.keymaps")

class SelectKeymap(step.InteractiveStep):
	first_install = True

	def run(self):
		# Get a list of all available keymaps
		keymaps = self.bricklayer.command(["localectl", "list-keymaps"], error_ok=True)

		# Skip this step if no keymaps could be loaded
		if not keymaps:
			return

		# Split the output by line
		keymaps = { km : km for km in keymaps.splitlines() }

		# Which keymap is currently selected?
		keymap = self.bricklayer.settings.get("keymap")

		# Ask the user to select a keymap
		keymap = self.tui.select(
			_("Keyboard Layout"),
			_("Please select your keyboard layout:"),
			keymaps, default=keymap, height=10,
		)

		log.info("Selected keymap: %s" % keymap)

		# Save in settings
		self.bricklayer.settings["keymap"] = keymap


class ApplyKeymap(step.Step):
	"""
		Applies the keymap
	"""
	def run(self):
		keymap = self.bricklayer.settings.get("keymap")

		# Ignore any errors here, because we might not be able to run this on
		# a foreign host system.
		if keymap:
			self.bricklayer.command(["localectl", "set-keymap", keymap], error_ok=True)
