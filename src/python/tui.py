###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import snack

from .errors import *
from .i18n import _

# Setup logging
log = logging.getLogger("bricklayer.tui")

def abort():
	raise InstallAbortedError

def cancel():
	raise UserCanceledError

class Tui(object):
	def __init__(self, bricklayer):
		self.bricklayer = bricklayer

		# Placeholder for screen
		self.screen = None

	# Make this class usable as context

	def __enter__(self):
		log.debug("Entering TUI context")

		# Setup the screen
		self._setup_screen()

		return self

	def __exit__(self, type, value, traceback):
		log.debug("Leaving TUI context")

		# Wipe the screen
		self._finish_screen()

	def shell(self):
		"""
			Opens a debug shell
		"""
		if not self.screen:
			return

		# Hide the installer
		self.screen.suspend()

		# Show a message how to return to the installer
		print(_("Type <exit> to return to the install program\n"))

		self.bricklayer.command(["/bin/bash", "--login"], interactive=True, error_ok=True)

		# Bring back the installer
		self.screen.resume()

	def refresh(self):
		"""
			Refreshes what is written on the screen
		"""
		if self.screen:
			self.screen.refresh()

	def _setup_screen(self):
		"""
			Sets up the screen
		"""
		self.screen = snack.SnackScreen()

		# Setup root text
		self.draw_root_text("%(PRETTY_NAME)s" % self.bricklayer.os)

		# Setup helpline
		helpline = []

		# Show how to get to shell in debug mode
		if self.bricklayer.debug:
			helpline.append(
				_("<Ctrl-Z> Shell")
			)

		# Show key commands only when they are useful
		if not self.bricklayer.unattended:
			helpline += (
				_("<Tab>/<Alt-Tab> between elements"),
				_("<Space> selects"),
				_("<F12> next screen"),
			)

		self.push_helpline(" | ".join(helpline))

		# Refresh the screen
		self.refresh()

		# Set the suspend callback
		self.screen.suspendCallback(self.shell)

	def _finish_screen(self):
		"""
			Cleanup screen
		"""
		if self.screen:
			self.screen.finish()
			self.screen = None

	def draw_root_text(self, text, top=0, left=0):
		if not self.screen:
			raise RuntimeError()

		# Center the text
		text = text.center(self.screen.width)

		# Draw the text
		self.screen.drawRootText(top, left, text)

	def push_helpline(self, helpline):
		"""
			Sets the helpline, but centers it first
		"""
		if not self.screen:
			raise RuntimeError()

		# Center the string
		if self.screen.width:
			helpline = helpline.center(self.screen.width)

		self.screen.pushHelpLine(helpline)

	def message(self, title, text, buttons=None, height=None, width=40,
			help=None, timeout=None):
		"""
			Shows a message to the user
		"""
		assert self.screen

		# Set default buttons
		if buttons is None:
			buttons = (_("OK"), _("Cancel"))

		window = ButtonsWindow(self, title=title, text=text,
			buttons=buttons, height=height, width=width, help=help, timeout=timeout)

		return window.run()

	def error(self, title, text, buttons=None, width=40):
		if not buttons:
			buttons = [
				(_("Abort Installation"), abort),
			]

		return self.message(title, text, buttons=buttons, width=width)

	def confirm(self, title, text, height=None, width=40, help=None):
		"""
			Asks the user a yes/no question
		"""
		buttons = (
			(_("No"), False),
			(_("Yes"), True),
		)

		return self.message(title, text, buttons=buttons,
			height=height, width=width, help=help)

	def progress(self, title, text, max_value=None, height=None, width=60, help=None):
		return ProgressWindow(self, title, text, max_value=max_value,
			height=height, width=width, help=help)

	def select(self, title, text, items, default=None, multi=False, buttons=None,
			height=None, width=40, help=None):
		window = SelectWindow(self, title, text, items, default=default, multi=multi,
			buttons=buttons, height=height, width=width, help=help)

		return window.run()

	def passwd(self, title, text, height=None, width=40, help=None):
		window = PasswordWindow(self, title, text, height=height, width=width, help=help)

		return window.run()


class Window(object):
	def __init__(self, tui, title, text, height=None, width=None, help=None):
		self.tui = tui
		self.title = title
		self.text = text
		self.height = height
		self.width = width
		self.help = help

	@property
	def max_height(self):
		"""
			The maximum height of any window
		"""
		return self.tui.screen.height - 12

	def run(self):
		raise NotImplementedError


class ButtonsWindow(Window):
	def __init__(self, tui, title, text, buttons=None, height=None, width=None,
			help=None, timeout=None):
		Window.__init__(self, tui, title, text, height=height, width=width, help=help)
		self.timeout = timeout

		# Configure some default buttons
		if buttons is None:
			buttons = self.default_buttons

		self.buttons = snack.ButtonBar(self.tui.screen, buttons)

	@property
	def default_buttons(self):
		return (
			(_("Ok"), None),
			(_("Cancel"), cancel),
		)

	def _make_window(self):
		# Create a grid
		grid = snack.GridFormHelp(self.tui.screen, self.title, self.help, 1, 2)

		# Create the box that shows the text
		textbox = snack.TextboxReflowed(self.width, self.text, maxHeight=self.max_height)
		grid.add(textbox, 0, 0, padding=(0, 0, 0, 1))

		# Create the button bar
		grid.add(self.buttons, 0, 1, growx=True)

		# Set timeout
		if self.timeout:
			grid.setTimer(self.timeout * 1000)

		return grid

	def run(self):
		window = self._make_window()

		# Run the window in a loop. The callback must return a value, otherwise the lo
		try:
			while True:
				button = window.run()

				# Which button was pressed?
				callback = self.buttons.buttonPressed(button)

				try:
					# If the button had a callback, we will call the callback
					if callable(callback):
						return callback()

					# Otherwise call the default action
					elif callback is None:
						return self.default_action()

					# Or just return the value
					return callback

				# If any of the callbacks raise TryAgain, we go back and let the user
				# edit the form again
				except TryAgainError:
					continue
		finally:
			self.tui.screen.popWindow()

	def default_action(self):
		"""
			Called when a button did not have a callback
		"""
		pass


class SelectWindow(ButtonsWindow):
	def __init__(self, tui, title, text, items, default=None, multi=False, buttons=None,
			height=None, width=None, help=None):
		# Set height to number of items by default
		if height is None:
			height = len(items)

		# Set some default buttons
		if buttons is None:
			buttons = [
				(_("Select"), None),
				(_("Cancel"), cancel),
			]

		super().__init__(tui, title, text, buttons=buttons,
			height=height, width=width, help=help)

		self.items = items
		self.default = default
		self.multi = multi

		# Should we enable scrolling?
		scroll = height < len(items)

		# Create the list box
		if self.multi:
			self.listbox = snack.CheckboxTree(self.height, scroll=scroll)
			for key in self.items:
				self.listbox.append(self.items[key], key,
					key in default if default else False)
		else:
			self.listbox = snack.Listbox(self.height, scroll=scroll, returnExit=True)
			for key in self.items:
				self.listbox.append(self.items[key], key)

			# Set the default value
			if self.default:
				self.listbox.setCurrent(self.default)

	def _make_window(self):
		# Create a grid
		grid = snack.GridFormHelp(self.tui.screen, self.title, self.help, 1, 3)

		# Create the text box
		textbox = snack.TextboxReflowed(self.width, self.text)
		grid.add(textbox, 0, 0)

		# Add the listbox
		grid.add(self.listbox, 0, 1, padding=(0, 1, 0, 1))

		# Add the buttons
		grid.add(self.buttons, 0, 2, growx=True)

		return grid

	def default_action(self):
		# Return the selected item(s)
		if self.multi:
			return self.listbox.getSelection()

		return self.listbox.current()


class ProgressWindow(Window):
	"""
		Shows a notification (optionally with a progress bar) about what is going on
	"""
	def __init__(self, tui, title, text, max_value=0, height=None, width=None, help=None):
		Window.__init__(self, tui, title, text, height=height, width=width, help=help)

		self.max_value = max_value

		# Make textbox
		self.textbox = snack.TextboxReflowed(self.width, self.text)

		# Make progressbar
		self.scale = snack.Scale(self.width, self.max_value or 0)

	def _make_window(self):
		# Create the grid
		grid = snack.GridFormHelp(self.tui.screen, self.title, self.help, 1, 2)

		# Add the textbox
		grid.add(self.textbox, 0, 0)

		# Optionally add the progress bar
		if self.max_value:
			grid.add(self.scale, 0, 1, padding=(0, 1, 0, 0))

		return grid

	def __enter__(self):
		# Render the window
		window = self._make_window()
		window.draw()

		self.tui.refresh()

		# Return the callbacks
		return ProgressWindowCallbacks(self)

	def __exit__(self, type, value, traceback):
		self.tui.screen.popWindow()


class ProgressWindowCallbacks(object):
	def __init__(self, window):
		self.window = window

	def progress(self, value):
		"""
			Updates the progressbar value
		"""
		if self.window.max_value:
			self.window.scale.set(value)
			self.window.tui.refresh()

	def message(self, text):
		"""
			Updates the text in the textbox
		"""
		self.window.textbox.setText(text)
		self.window.tui.refresh()

	def status(self, progress, message):
		if progress:
			self.progress(progress)

		if message:
			self.message(message)


class PasswordWindow(ButtonsWindow):
	@property
	def default_buttons(self):
		return (
			(_("Set Password"), self.set_password),
			(_("Cancel"), cancel),
		)

	def _make_window(self):
		# Create a grid
		grid = snack.GridFormHelp(self.tui.screen, self.title, self.help, 1, 3)

		# Create the box that shows the text
		textbox = snack.TextboxReflowed(self.width, self.text, maxHeight=self.max_height)
		grid.add(textbox, 0, 0, padding=(0, 0, 0, 1))

		# Create password fields
		passwords = snack.Grid(2, 2)

		self.password1 = snack.Entry(24, password=True)
		label = snack.Label(_("Password"))

		passwords.setField(label, 0, 0, padding=(0, 0, 1, 0), anchorLeft=True)
		passwords.setField(self.password1, 1, 0, anchorLeft=True)

		self.password2 = snack.Entry(24, password=True)
		label = snack.Label(_("Confirm"))

		passwords.setField(label, 0, 1, padding=(0, 0, 1, 0), anchorLeft=True)
		passwords.setField(self.password2, 1, 1, anchorLeft=True)

		grid.add(passwords, 0, 1, padding=(0, 0, 0, 1))

		# Create the button bar
		grid.add(self.buttons, 0, 2, growx=True)

		return grid

	def set_password(self):
		# Fetch the entered values
		password1 = self.password1.value()
		password2 = self.password2.value()

		# Has something been entered?
		if not password1:
			self.tui.message(
				_("Error"),
				_("You must enter a password"),
				buttons=[_("OK")],
			)
			raise TryAgainError

		# Do the passwords match?
		if not password1 == password2:
			self.tui.message(
				_("Error"),
				_("The entered passwords do not match"),
				buttons=[_("OK")],
			)
			raise TryAgainError

		# Return the password to the caller
		return password1
