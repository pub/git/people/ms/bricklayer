###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import os
import time
import pakfire.errors

import snack

from . import i18n
from .errors import *
from .i18n import _

# Setup logging
log = logging.getLogger("bricklayer.step")

class Step(object):
	"""
		Steps are the heart of this installer. They perform actions and are
		being executed one after the other.
	"""
	# This enables or disables this step
	enabled = True

	# Should this be run in first-install mode?
	first_install = False

	def __init__(self, bricklayer, tui):
		self.bricklayer = bricklayer
		self.tui = tui

		log.debug("Initializing step %s" % self.__class__.__name__)

		# Call the custom initialization
		self.initialize()

	def initialize(self):
		"""
			Custom initialization action to be overlayed
		"""
		pass

	def run(self):
		"""
			Run this step - to be overlayed
		"""
		pass

	def install_packages(self, packages, title=None):
		# Nothing to do if there are no packages
		if not packages:
			return

		log.debug("Installing packages: %s" % ", ".join(packages))

		# Set a useful title if none is set
		if not title:
			title = _("Installing Packages")

		# Set up Pakfire
		with self.tui.progress(title, _("Installing packages..."), max_value=100) as t:
			# Create a new Pakfire instance
			p = self.bricklayer.setup_pakfire()

			try:
				# Install packages
				p.install(packages, status_callback=t.status)

			# Abort on any dependencies problems
			except pakfire.errors.DependencyError as e:
				problems = e.args[0]

				# Format problem descriptions
				text = []
				for problem in problems:
					lines = [
						"* %s" % problem
					]
					for solution in problem.solutions:
						lines.append("  --> %s" % solution)

					text.append("\n".join(lines))

				self.tui.error(
					_("Dependency Problem"),
					_(
						"A problem has occured during resolving package dependencies:\n\n%s",
						"Problems have occured during resolving package dependencies:\n\n%s",
						len(problems),
					) % "\n\n".join(text),
					width=78,
				)


class InteractiveStep(Step):
	"""
		A convenience handler that is disabled in unattended mode
	"""
	@property
	def enabled(self):
		# Disable in unattended mode
		return not self.bricklayer.unattended


class UnattendedStep(Step):
	"""
		A convenience handler that only runs this step in unattended mode
	"""
	@property
	def enabled(self):
		# Enable only in unattended mode
		return self.bricklayer.unattended


class Welcome(InteractiveStep):
	"""
		Shows a very warm welcome message to the user
	"""
	first_install = True

	def run(self):
		name = self.bricklayer.os.get("NAME")

		# Fetch the currently selected locale
		locale = self.bricklayer.settings.get("locale", i18n.DEFAULT_LOCALE)

		# Let the user select
		locale = self.tui.select(
			_("Willkommen, Bienvenue, Welcome!"),
			_("Select the language you wish to use for the installation"),
			i18n.SUPPORTED_LOCALES, default=locale,
			buttons=[_("Start Installation")], width=60,
		)

		log.info("Locale selected: %s" % locale)

		# Store in settings
		self.bricklayer.settings["locale"] = locale

		# Set to environment
		os.environ["LANGUAGE"] = locale


class Congratulations(Step):
	"""
		Shows a message that the installation is complete
	"""
	first_install = True

	def run(self):
		if self.bricklayer.unattended:
			message = _("The unattended installation process has been completed.")
		else:
			message = _(
				"The installation has been completed successfully."
				"\n\n"
				"You can now safely remove any media used for the installation."
			)

		self.tui.message(
			_("Congratulations"), message, buttons=[_("Finish")],

			# Make the window a litte bit bigger
			width=50,

			# Automatically continue in unattended mode
			timeout=10 if self.bricklayer.unattended else None,
		)


class UnattendedWarning(UnattendedStep):
	def run(self):
		timeout = 10
		disks = self.bricklayer.disks.selected

		message = _(
			"The unattended installation will start in %(timeout)s seconds using %(disks)s",
		) % {
			"timeout" : timeout,
			"disks"   : i18n.list(disks),
		}

		# Show message to the user and allow them to cancel
		if self.tui.message(_("Unattended Installation"), message,
			buttons=[_("Cancel Unattended Installation")], timeout=timeout):
				raise InstallAbortedError


class RootPassword(InteractiveStep):
	first_install = True

	def run(self):
		password = self.tui.passwd(
			_("Root Password"),
			_("Please enter the password for the 'root' user"),
		)

		# Save the password for now
		self.bricklayer.settings["root-password"] = password


class SetRootPassword(Step):
	def run(self):
		with self.tui.progress(_("Applying Configuration"),
				_("Setting password for the '%s' user...")) as t:
			p = self.bricklayer.setup_pakfire()

			# XXX execute currently has no way to write to stdin, which is
			# why we reset the root password here, so that we will have a working login
			p.execute(["passwd", "--delete", "root"])


class ToggleFirstInstallStatus(Step):
	first_install = True

	@property
	def enabled(self):
		# Only enabled in unattended and first install mode
		return self.bricklayer.unattended or self.bricklayer.first_install

	def run(self):
		# Path to the marker file
		path = os.path.join(self.bricklayer.root, ".firstinstall")

		# Create file in unattended mode
		if self.bricklayer.unattended:
			with open(path, "w"):
				pass

		# Remove the file in first install mode
		elif self.bricklayer.first_install:
			try:
				os.unlink(path)
			except FileNotFoundError:
				pass
