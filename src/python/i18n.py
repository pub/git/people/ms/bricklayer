###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import gettext

SUPPORTED_LOCALES = {
	"de_DE.UTF-8" : "Deutsch",
	"en_US.UTF-8" : "English (United States)",
}

DEFAULT_LOCALE = "en_US.UTF-8"

# Check if the default locale is supported
assert DEFAULT_LOCALE in SUPPORTED_LOCALES, "DEFAULT_LOCALE is not supported"

N_ = lambda x: x

def _(singular, plural=None, n=None):
	if plural:
		return gettext.dngettext("bricklayer", singular, plural, n)

	return gettext.dgettext("bricklayer", singular)

def list(objects):
	length = len(objects)

	if length == 0:
		return ""
	elif length == 1:
		return objects[0]
	else:
		return _("%(commas)s, and %(last)s") % {
			"commas" : ",".join(objects[:-1]),
			"last"   : objects[-1],
		}
