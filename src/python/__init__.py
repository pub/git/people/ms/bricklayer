###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import os
import shlex
import subprocess
import sys
import tempfile
import traceback

import pakfire

from . import bootloaders
from . import disk
from . import i18n
from . import keymaps
from . import logger
from . import packages
from . import step
from . import timezones
from . import tui
from . import util
from .i18n import _
from .errors import *

# Setup logging
log = logging.getLogger("bricklayer")

class Bricklayer(object):
	"""
		Bricklayer's base class
	"""
	def __init__(self, arch, pakfire_conf=None, first_install=False, debug=False,
			unattended=False, disks=[], serial=False, ignore_kernel_cmdline=False):
		self.arch = arch
		self.pakfire_conf = pakfire_conf
		self.first_install = first_install
		self.debug = debug
		self.unattended = unattended

		# Enable debug logging
		if debug:
			log.setLevel(logging.DEBUG)

		# Settings
		self.settings = {
			"locale" : i18n.DEFAULT_LOCALE,
			"keymap" : "us",

			"packages" : [
				"system-release",
			],

			# Set the default swap size to 1 GiB
			"swap-size": 1024 ** 3,

			# Default timezone
			"timezone" : "UTC",

			# Kernel Commandline
			"kernel-cmdline" : ["quiet", "splash"],

			# Serial Console
			"serial-console"          : serial,
			"serial-console-device"   : "ttyS0",
			"serial-console-baudrate" : 115200,
		}

		# Parse the kernel command line
		if ignore_kernel_cmdline:
			self._read_cmdline()

		# Read OS information
		self.os = self._read_os_release()

		# Select a bootloaders
		self.bootloaders = self._select_bootloaders()

		# Hardware
		self.disks = disk.Disks(self)
		for path in disks:
			self.disks.add_disk(path, selected=True)

		# Initialise the text user interface
		self.tui = tui.Tui(self)

		# Create root directory
		self.root = tempfile.mkdtemp(prefix="bricklayer-", suffix="-root")

		# Log when we are ready
		log.info("Bricklayer initialized")

	# An ordered list of all available steps
	steps = (
		step.Welcome,
		keymaps.SelectKeymap,
		keymaps.ApplyKeymap,
		timezones.SelectTimezone,
		disk.Scan,
		disk.UnattendedSelectDisk,
		disk.SelectDisk,
		disk.CalculatePartitionLayout,
		step.RootPassword,

		# Go!
		step.UnattendedWarning,
		disk.CreatePartitionLayout,
		disk.CreateFilesystems,
		disk.MountFilesystems,
		packages.InstallPackages,
		step.SetRootPassword,
		disk.WriteFilesystemTable,
		bootloaders.InstallBootloader,

		# Done!
		step.ToggleFirstInstallStatus,
		disk.UmountFilesystems,
		step.Congratulations,
	)

	def __call__(self):
		with self.tui:
			try:
				# Walk through all steps
				for step in self.steps:
					try:
						self._run_step(step)

					# End installer if aborted
					except InstallAbortedError:
						return 1

					# The user requested to cancel the installation process
					except UserCanceledError:
						if self.tui.confirm(
							_("Cancel Installation?"),
							_("Are you sure that you want to cancel the installation process?"),
						):
							return 0

					# Catch any failed commands
					except subprocess.CalledProcessError as e:
						args = {
							"command"    : " ".join(e.cmd),
							"output"     : e.output.decode(),
							"returncode" : e.returncode,
						}

						# Log the error
						log.error("Command \"%(command)s\" failed with error code "
							"%(returncode)s:\n%(output)s" % args)

						# Format the error message
						error = _("Command \"%(command)s\" failed with error code "
							"%(returncode)s:\n\n%(output)s" % args)

						# Show it
						self.tui.error(_("An Unexpected Error Occured"), error,
							buttons=[_("Exit")], width=78)

						# Exit
						return e.returncode

					# Catch all other exceptions and show an error
					except:
						type, value, tb = sys.exc_info()

						# Log the error
						log.error("An unexpected error occured:", exc_info=True)

						# Format the exception
						error = _("The installation cannot be continued due to an error:"
							"\n\n%s") % "".join(traceback.format_exception(type, value, tb))

						# Show an error message
						self.tui.error(_("An Unexpected Error Occured"),
							"".join(error), buttons=[_("Exit")], width=78)

						# Exit
						return 1

			# Cleanup when we leave
			finally:
				self.disks.tear_down()

	def _run_step(self, stepcls):
		"""
			Runs a single step
		"""
		# Initialize the step
		step = stepcls(self, tui=self.tui)

		# Skip this step if it isn't enabled in first install mode
		if self.first_install and not step.first_install:
			return

		# Skip this step if it isn't enabled
		if not step.enabled:
			return

		# Run it
		return step.run()

	def _read_os_release(self):
		"""
			Read /etc/os-release
		"""
		with open("/etc/os-release") as f:
			return util.config_read(f)

	def open(self, path, *args, **kwargs):
		"""
			Opens a file inside the environment
		"""
		# Make the path relative
		while path.startswith("/"):
			path = path[1:]

		# Make the path relative to the root directory
		path = os.path.join(self.root, path)

		# Open the file and return the handle
		return open(path, *args, **kwargs)

	def command(self, command, error_ok=False, interactive=False, chroot=False, bind=None):
		"""
			Runs a command in a shell environment
		"""
		log.debug("Running command: %s" % " ".join(command))

		# Run this command in the installed environment if requested
		if chroot:
			command = ["chroot", self.root] + command

		args = {}

		if not interactive:
			args.update({
				"stdout" : subprocess.PIPE,
				"stderr" : subprocess.STDOUT,
			})

		try:
			# Bind-mount things
			if bind:
				self._bind(bind)

			# Execute the command
			p = subprocess.run(command, **args)

		finally:
			# If the command is successful or not, we will always need to unbind
			# anything that has been mounted before.
			if bind:
				self._unbind(bind)

		# Skip any output processing on error
		if p.returncode and error_ok:
			return

		# Check the return code (raises CalledProcessError on non-zero)
		elif not error_ok:
			p.check_returncode()

		# There is no output in interactive mode
		if interactive:
			return

		# Decode output
		output = p.stdout.decode()

		# Log output
		if output:
			log.debug(output)

		return output

	def _bind(self, mountpoints):
		"""
			Bind-mounts the given mountpoints
		"""
		for source in mountpoints:
			if not source.startswith("/"):
				raise ValueError("Mountpoints must be absolute paths")

			target = os.path.join(self.root, source[1:])

			# Make sure we mount into our root
			assert(target.startswith(self.root))

			# Perform mount
			self.command(["mount", "--bind", source, target])

	def _unbind(self, mountpoints):
		"""
			Umounts any bind-mounted mountpoints
		"""
		for source in reversed(mountpoints):
			if not source.startswith("/"):
				continue

			target = os.path.join(self.root, source[1:])

			# Make sure we mount into our root
			assert(target.startswith(self.root))

			# Perform umount
			self.command(["umount", target], error_ok=True)

	def setup_pakfire(self, **kwargs):
		"""
			Calls Pakfire and has it load its configuration
		"""
		return pakfire.Pakfire(self.root, arch=self.arch,
			conf=self.pakfire_conf, **kwargs)

	def _select_bootloaders(self):
		"""
			Select all bootloaders for this installation process
		"""
		_bootloaders = (
			bootloaders.Grub(self),
			bootloaders.GrubEFI(self),
		)

		# Return all supported bootloaders
		return [bl for bl in _bootloaders if bl.supported]

	def _read_cmdline(self):
		"""
			This function parses the kernel command line
		"""
		with open("/proc/cmdline") as f:
			# Read the entire content
			line = f.readline()

		# Process line for line...
		for word in shlex.split(line):
			# Split each word
			key, delim, value = word.partition("=")

			# Parse console= options
			if key == "console":
				self._cmdline_handle_console(value)

			# Activate unattended mode
			elif key == "installer.unattended":
				self.unattended = True

			# Handle any installer.* keywords
			elif key.startswith("installer."):
				self._cmdline_handle_keyword(key[10:], value)

	def _cmdline_handle_keyword(self, key, value):
		"""
			Sets any configuration values from the commandline
		"""
		self.settings[key] = value

	def _cmdline_handle_console(self, value):
		"""
			Parses the console= kernel parameter
		"""
		# Fetch the first part which is the device name
		device, delim, options = value.partition(",")

		# Break if this option does NOT configure a serial console
		if not device.startswith("ttyS"):
			return

		# This function does not properly handle all cases, but I think this
		# is good enough for now...

		# Remove the "n8" suffix
		options = options.removesuffix("n8")

		# The rest should
		try:
			baudrate = int(options)
		except (TypeError, ValueError):
			baudrate = None

		# Store everything in settings
		self.settings |= {
			"serial-console"        : True,
			"serial-console-device" : device,
		}

		# Store baudrate if set
		if baurate:
			self.settings["serial-console-baudrate"] = baudrate
