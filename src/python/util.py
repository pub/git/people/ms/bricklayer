###############################################################################
#                                                                             #
# Bricklayer - An Installer for IPFire                                        #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software; you can redistribute it and/or               #
# modify it under the terms of the GNU General Public License                 #
# as published by the Free Software Foundation; either version 2              #
# of the License, or (at your option) any later version.                      #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

def config_read(f):
	"""
		Read a configuration file in key/value format
	"""
	res = {}

	# Read lines and split them by key/value
	for line in f:
		# Strip any trailing newline
		line = line.rstrip()

		# Split key/value
		key, delim, value = line.partition("=")

		# Unquote value
		if value and value[0] == "\"" and value[-1] == "\"":
			value = value[1:-1]

		# Store the value
		res[key] = value

	return res

def format_size(s):
	units = (
		"%.0f ",
		"%.0fk",
		"%.1fM",
		"%.1fG",
		"%.1fT",
		"%.1fP",
		"%.1fE",
	)

	unit = None

	for unit in units:
		if abs(s) < 1024:
			break

		s /= 1024

	return unit % s
